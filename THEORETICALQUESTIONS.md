1. Опишіть своїми словами що таке Document Object Model (DOM).

Це деровоподібне відображення веб-сторінки у вигляді вузлів-об'єктів, які можна змінювати.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

innerHTML додає або змінює HTML структуру елемента, можна створювати нові елементи.

innerText змінює або додає текст, без зміни структури. Якщо записати тег, то він буде відображатись як текстовий рядок.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

getElementById(),
getElementsByClassName(),
getElementsByTagName(),
getElementsByName
querySelector(),
quarySelectorAll().

Найбільш оптимальний та гнучкий із них це quarySelectorAll().
