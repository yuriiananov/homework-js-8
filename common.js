"use strict";

// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000

const paragraph = document.getElementsByTagName("p");
for (let i = 0; i < paragraph.length; i++) {
    paragraph[i].style.background = '#ff0000';
}

/* 2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.*/

const optionsList = document.getElementById("optionsList"); 
console.log(optionsList);

const parentElement = optionsList.parentElement;
console.log(parentElement);

const childNodes = optionsList.childNodes;
for (let i = 0; i < childNodes.length; i++) {
    let childNode = childNodes[i];

    console.log(childNode.nodeName, childNode.nodeType);
}

// 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph

const testParagraph = document.querySelector("#testParagraph");
testParagraph.innerHTML = "<p>This is a paragraph</p>";

// 4. Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

const elements = document.querySelector(".main-header");
const allElements = elements.querySelectorAll("*"); 
console.log(allElements);
for (let i = 0; i < allElements.length; i++) {
    allElements[i].classList.add("nav-item");
    //allElements[i].className = "nav-item";
}

// 5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

const allElem = document.querySelectorAll(".section-title");
for (let i = 0; i < allElem.length; i++) {
    allElem[i].classList.toggle("section-title");
    //allElem[i].classList.remove("section-title");
}